//
//  MessengerListen.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class MessengerListen {
    class var sharedInstance: MessengerListen {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: MessengerListen? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = MessengerListen()
        }
        return Static.instance!
    }
    
    var messages: Array<Message> = []
    
    func addMessage(message: Message) {
        self.messages.append(message)
        
        
    }
}