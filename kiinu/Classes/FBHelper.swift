//
//  FBHelper..swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FBHelper {
    
    
    static func send (jsonMessage: [String: AnyObject]) {
        
        Manager.sharedInstance.request(.POST,
            "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send",
            parameters: jsonMessage,
            encoding: .URL).validate()
            .response{(request, response, jsonData, error) -> Void in
                let json: JSON = JSON(data:jsonData!)
                print ("json: \(json)")
                print ("error: \(error)")
        }
    }
    
    static func sendTextMessage (message: Message) {
        let json = ["text": message.text!]
        self.send(json)
//        self.sendMessage(json)
    }
    
    static func sendTextMessageAndButton (message: ButtonsMessage) {
        Manager.sharedInstance.request(.POST,
            "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-buttons",
            parameters: nil,
            encoding: .URL).validate()
            .response{(request, response, jsonData, error) -> Void in
                let json: JSON = JSON(data:jsonData!)
                print ("json: \(json)")
                print ("error: \(error)")
        }
    }
    
    static func sendProducts2 () {
        Manager.sharedInstance.request(.POST,
            "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-products-2",
            parameters: nil,
            encoding: .URL).validate()
            .response{(request, response, jsonData, error) -> Void in
                let json: JSON = JSON(data:jsonData!)
                print ("json: \(json)")
                print ("error: \(error)")
        }
    }
    
    static func sendProducts () {
        Manager.sharedInstance.request(.POST,
            "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-products-1",
            parameters: nil,
            encoding: .URL).validate()
            .response{(request, response, jsonData, error) -> Void in
                let json: JSON = JSON(data:jsonData!)
                print ("json: \(json)")
                print ("error: \(error)")
        }
    }
    
    static func sendUrber () {
        Manager.sharedInstance.request(.POST,
            "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-urber",
            parameters: nil,
            encoding: .URL).validate()
            .response{(request, response, jsonData, error) -> Void in
                let json: JSON = JSON(data:jsonData!)
                print ("json: \(json)")
                print ("error: \(error)")
        }
    }
    
    static func sendPrice () {
        Manager.sharedInstance.request(.POST,
            "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-prices",
            parameters: nil,
            encoding: .URL)
    }
    
    static func sendSizes () {
        Manager.sharedInstance.request(.POST,
                                       "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-sizes",
                                       parameters: nil,
                                       encoding: .URL)
    }
    
    static func sendStyles () {
        Manager.sharedInstance.request(.POST,
                                       "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-styles",
                                       parameters: nil,
                                       encoding: .URL)
    }
    
    static func sendPayment () {
        Manager.sharedInstance.request(.POST,
                                       "http://immense-bastion-49377.herokuapp.com/api/receivers/\(MessengerID)/send-request-payment",
                                       parameters: nil,
                                       encoding: .URL)
    }
    
    
    private static func sendMessage(jsonMessage: AnyObject) {
        let token = "EAADuRZBPvA8kBALzZCo0NuqGGJPkyd8Fji31pD330rkEoennB6kkHfbBZCnRjjMzpuhsdP19tXr2nEDa0i5ONKhqA6TDQvZAHZAP8iaFsVw7SObM34MOvD3bVoPZCQ4nW0GpRGQV6x9cZBNuKeujVBjyWUhMtMV9eux5CVZAzUhfHQZDZD"
        let api = "https://graph.facebook.com/v2.6/me/messages?access_token=\(token)"
        
        Manager.sharedInstance.request(.POST,
                                       api,
                                       parameters: ["recipient" : ["id" : MessengerID], "message" : jsonMessage ],
            encoding: .URL).validate()
            .response{(request, response, jsonData, error) -> Void in
                let json: JSON = JSON(data:jsonData!)
                print ("json: \(json)")
                print ("error: \(error)")
        }
    }
    
}

