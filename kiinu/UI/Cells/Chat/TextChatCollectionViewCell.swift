//
//  TextChatViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class TextChatCollectionViewCell: ChatCollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
 
    override func layoutMessage() {
        super.layoutMessage()
        
        self.textLabel.text = self.message?.text
        
        if self.isLeft {
            self.textLabel.textAlignment = .Left
            self.textLabel.textColor = UIColor.whiteColor()
            self.leftContentChatviewConstraint.constant = 10
            self.contentChatView.backgroundColor = UIColor(red:0.14, green:0.62, blue:0.90, alpha:1.00)
            
        } else {
            self.textLabel.textAlignment = .Right
            self.textLabel.textColor = UIColor(red:0.47, green:0.47, blue:0.47, alpha:1.00)
            
            self.leftContentChatviewConstraint.constant = ScreenSize.Width - 20 - self.contentChatView.width
            self.contentChatView.backgroundColor = UIColor.whiteColor()
        }
    }
}