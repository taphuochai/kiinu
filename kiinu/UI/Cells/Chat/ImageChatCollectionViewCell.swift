//
//  ImageChatViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class ImageChatCollectionViewCell: ChatCollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func layoutMessage() {
        super.layoutMessage()
        
        let imageMessage = self.message as! ImageMessage
        self.imageView.image = imageMessage.image
    }
}