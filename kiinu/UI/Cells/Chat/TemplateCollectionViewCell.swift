//
//  TemplateCollectionViewCell.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class TemplateChatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contentChatView: UIView!
    
    var _message: Message?
    var message: Message? {
        get { return _message }
        set (value) {
            _message = value
        }
    }
    
    override func awakeFromNib() {
        self.contentChatView.layer.cornerRadius = 15
        self.contentChatView.clipsToBounds = true
    }
    
    func layoutMessage() {}
}