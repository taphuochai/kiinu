
//
//  ProductCollectionViewCell.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contentProductView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var viewMoreButton: UIButton!
    
    private var _product: Product?
    var product: Product? {
        get { return _product }
        set(value) {
            _product = value
            
            if value != nil {
                self.titleLabel.text = _product?.name
                self.imageView.sd_setImageWithURL(NSURL(string: (_product?.imageUrl)!))
            }
        }
    }
    
    override func awakeFromNib() {
        self.contentProductView.layer.cornerRadius = 4
        self.contentProductView.clipsToBounds = true
        
        self.buyButton.layer.borderColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.00).CGColor
        self.buyButton.layer.borderWidth = 0.5
        self.buyButton.layer.cornerRadius = 2
        
        self.viewMoreButton.layer.borderColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.00).CGColor
        self.viewMoreButton.layer.borderWidth = 0.5
        self.viewMoreButton.layer.cornerRadius = 2
    }
}