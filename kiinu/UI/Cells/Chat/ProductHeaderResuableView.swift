//
//  ProductHeaderResuableView.swift
//  kiinu
//
//  Created by taphuochai on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class ProductHeaderResuableView: UICollectionReusableView {
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        self.textField.layer.cornerRadius = self.textField.height/2
    }
}