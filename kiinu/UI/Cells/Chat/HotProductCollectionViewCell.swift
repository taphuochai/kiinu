//
//  HotProductCollectionViewCell.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import UIKit

class HotProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    
    private var _product: Product?
    var product: Product? {
        get { return _product }
        set(value) {
            _product = value
            
            if ((_product?.name) != nil) {
                self.titleLabel.text = _product?.name
            }
            if ((_product?.desc) != nil) {
                let price = _product?.price!
                self.descLabel.text = "$\(price)"
            }
            if ((_product?.imageUrl) != nil) {
                self.imageView.sd_setImageWithURL(NSURL(string: (_product?.imageUrl)!))
            }
        }
    }
    
    override func awakeFromNib() {
        self.checkImage.layer.cornerRadius = self.checkImage.width/2
        self.checkImage.clipsToBounds = true
        
        self.imageView.layer.cornerRadius = 2
        self.imageView.layer.borderWidth = 0.5
        self.imageView.layer.borderColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.00).CGColor
    }
    
}