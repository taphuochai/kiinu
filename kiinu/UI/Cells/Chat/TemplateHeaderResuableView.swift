//
//  TemplateHeaderResuableView.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import UIKit

class TemplateHeaderResuableView: UICollectionReusableView {
    @IBOutlet weak var label: UILabel!
}