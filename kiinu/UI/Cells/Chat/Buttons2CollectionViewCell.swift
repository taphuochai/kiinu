//
//  Buttons2CollectionViewCell.swift
//  kiinu
//
//  Created by taphuochai on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class Buttons2CollectionViewCell: ChatCollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bt1Button: UIButton!
    @IBOutlet weak var bt2Button: UIButton!
    
    override func layoutMessage() {
        super.layoutMessage()
        
        let bM = self.message as? ButtonsMessage
        
        self.titleLabel.text = bM?.text
        
        self.bt1Button.setTitle(bM?.buttons![0], forState: UIControlState.Normal)
        self.bt2Button.setTitle(bM?.buttons![1], forState: UIControlState.Normal)
    }
    
    override func awakeFromNib() {
        self.bt1Button.layer.borderColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.00).CGColor
        self.bt1Button.layer.borderWidth = 0.5
        self.bt1Button.layer.cornerRadius = 2
        
        self.bt2Button.layer.borderColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.00).CGColor
        self.bt2Button.layer.borderWidth = 0.5
        self.bt2Button.layer.cornerRadius = 2
    }
}