//
//  ChatCollectionViewCell.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import UIKit

class ChatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contentChatView: UIView!
    
    @IBOutlet weak var widthContentChatViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftContentChatviewConstraint: NSLayoutConstraint!
 
    var _message: Message?
    var message: Message? {
        get { return _message }
        set (value) {
            _message = value
            
            self.isLeft = (_message?.sender?.id)! == MessengerID
        }
    }
    
    var isLeft: Bool = true
    
    override func awakeFromNib() {
        self.contentChatView.layer.cornerRadius = 20
        
        self.contentChatView.layer.shadowRadius = 20
        self.contentChatView.layer.shadowOffset = CGSizeMake(4, 4)
        self.contentChatView.layer.shadowColor = UIColor(red:0.2, green:0.2, blue:0.2, alpha:0.8).CGColor
    }
    
    func layoutMessage() {}
}
