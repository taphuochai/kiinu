//
//  ButtonsTemplateCollectionView.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class ButtonsTemplateCollectionView: TemplateChatCollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bt1Button: UIButton!
    @IBOutlet weak var bt2Button: UIButton!
    @IBOutlet weak var bt3Button: UIButton!
    
    override func awakeFromNib() {
        for btn in [bt1Button, bt2Button, bt3Button] {
            btn.layer.cornerRadius = btn.height/2
            btn.clipsToBounds = true
        }
        
        self.contentChatView.layer.cornerRadius = 10
        self.contentChatView.clipsToBounds = true
    }
}