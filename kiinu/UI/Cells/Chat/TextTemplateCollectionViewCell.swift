//
//  TextTemplateCollectionViewCell.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class TextTemplateCollectionViewCell: ChatCollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
    
    override func layoutMessage() {
        super.layoutMessage()
        
        self.textLabel.text = self.message?.text
    }
}