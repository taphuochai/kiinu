//
//  GroupListCell.swift
//  kiinu
//
//  Created by Duc iOS on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit

class GroupListCell: BaseCell {
    @IBOutlet weak var btnCheckWidth: NSLayoutConstraint!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .None
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        btnCheckWidth.constant = selected ? 23 : 0
        layoutIfNeeded()
    }
}
