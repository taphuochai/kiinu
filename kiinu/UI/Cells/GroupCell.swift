//
//  GroupCell.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit

class GroupCell: UICollectionViewCell, UIActionSheetDelegate {
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var group: [String: String]! {
        didSet {
            lblName.text = group["name"]
            imgBg.image = UIImage(named: group["image"]!)
        }
    }
    var removeGroupBlock: ((group: [String: String])->Void)?
    var editGroupBlock: ((group: [String: String])->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 8
        clipsToBounds = true
    }
    @IBAction func didTapMore(sender: UIButton) {
        let ats = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil)
        ats.addButtonWithTitle("Edit")
        ats.addButtonWithTitle("Remove")
        ats.showInView(sender)
    }
    
    func actionSheet(actionSheet: UIActionSheet, didDismissWithButtonIndex buttonIndex: Int) {
        switch buttonIndex {
        case 1: // Edit
            editGroupBlock?(group: group)
        case 2: // Remove
            removeGroupBlock?(group: group)
        default:
            break
        }
    }
}
