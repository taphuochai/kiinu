//
//  HomeCell.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import TagListView
import SDWebImage

class HomeCell: BaseCell, TagListViewDelegate, UIActionSheetDelegate {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewTags: TagListView!
    @IBOutlet weak var viewTagsHeight: NSLayoutConstraint!
    @IBOutlet weak var btnAdd: UIButton!
    
    var addGroupBlock: ((conversation: [String: AnyObject])->Void)?
    var addTagsBlock: ((conversation: [String: AnyObject])->Void)?
    
    var conversation: [String: AnyObject]! {
        didSet {
            lblName.text = conversation["name"] as? String
            lblTime.text = conversation["time"] as? String
            lblMessage.text = conversation["message"] as? String
            imgView.sd_setImageWithURL(NSURL(string: conversation["avatar"] as! String))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgView.layer.cornerRadius = imgView.height/2
        imgView.clipsToBounds = true
        imgView.layer.shouldRasterize = true
        
        viewTags.delegate = self
        let n = arc4random_uniform(5)
        for _ in 0..<n {
            viewTags.addTag(Lorem.word)
        }
    }

    @IBAction func didTapAdd(sender: UIButton) {
        let act = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil)
        act.addButtonWithTitle("Add to Group")
        act.addButtonWithTitle("Add to Tags")
        act.showInView(sender)
    }
    
    func actionSheet(actionSheet: UIActionSheet, didDismissWithButtonIndex buttonIndex: Int) {
        switch buttonIndex {
        case 1:
            addGroupBlock?(conversation: conversation)
        case 2:
            addTagsBlock?(conversation: conversation)
        default:
            break
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        viewTagsHeight.constant = viewTags.intrinsicContentSize().height
    }
}
