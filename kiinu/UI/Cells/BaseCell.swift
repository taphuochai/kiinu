//
//  BaseCell.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layoutMargins = UIEdgeInsetsZero
    }
}
