//
//  AddEditGroupView.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit
import BFKit

class AddEditGroupView: UIView {
    
    @IBOutlet weak var txfName: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var group: [String: String]? {
        didSet {
            txfName.text = group?["name"]
        }
    }
    
    var colors = [["color": "d42525", "selected": true],
                  ["color": "e4ba29", "selected": false],
                  ["color": "21a1eb", "selected": false],
                  ["color": "3e55b9", "selected": false],
                  ["color": "10278b", "selected": false],
                  ["color": "ad09c0", "selected": false],
                  ["color": "51c3b2", "selected": false],
                  ["color": "1bc3ce", "selected": false],
                  ["color": "11c03f", "selected": false],
                  ["color": "1ea959", "selected": false],
                  ["color": "d9e925", "selected": false]]
    
    var closeBlock: (()->Void)?
    var doneBlock: ((oldGroup: [String: String]?, newGroup: [String: String])->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.registerClass(UICollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = UIColor.clearColor()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 42, height: 42)
        flowLayout.sectionInset = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 4)
        
        collectionView.collectionViewLayout = flowLayout
    }
    
    @IBAction func didTapClose(sender: UIButton) {
        closeBlock?()
    }
    
    @IBAction func didTapDone(sender: UIButton) {
        guard let name = txfName.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) where !name.isEmpty else {
            ALERT(nil, message: "Please enter group name.")
            return }
        
        doneBlock?(oldGroup: group, newGroup: ["name": name, "image": "group_icon_main_\(arc4random_uniform(4)+1)"])
    }

}

extension AddEditGroupView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        var btn = cell.viewWithTag(111) as? UIButton
        if btn == nil {
            btn = UIButton()
            btn?.tag = 111
            btn?.layer.cornerRadius = 21
            btn?.setImage(UIImage(named: "group_icon_check"), forState: .Selected)
            btn?.userInteractionEnabled = false
            cell.addSubview(btn!)
            btn?.autoPinEdgesToSuperviewEdges()
        }
        let color = colors[indexPath.item]
        btn?.backgroundColor = UIColor(hexString: color["color"] as! String)
        btn?.selected = color["selected"] as! Bool
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        for i in 0..<colors.count {
            colors[i]["selected"] = indexPath.item == i
        }
        collectionView.reloadData()
    }
}
