//
//  TagsViewController.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit
import ZFTokenField
import TagListView

class TagsViewController: BaseTableVC {
    @IBOutlet weak var tagsBg: UILabel!
    @IBOutlet weak var viewTags: ZFTokenField!
    @IBOutlet weak var viewTagsHeight: NSLayoutConstraint!
    @IBOutlet weak var viewSuggestionTags: TagListView!
    @IBOutlet weak var viewSuggestion: UIView!
    
    var tags = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "TAGS"
        
        viewTags.dataSource = self
        viewTags.delegate = self
        
        tagsBg.layer.cornerRadius = 16
        tagsBg.clipsToBounds = true
        
        let n = arc4random_uniform(7) + 1
        for _ in 0..<n {
            viewSuggestionTags.addTag(Lorem.word)
        }
        
        showHideSuggestions()
    }
    
    func updateTagsHeight() {
        viewTagsHeight.constant = viewTags.intrinsicContentSize().height
        view.layoutIfNeeded()
    }
    
    func showHideSuggestions() {
        let show = tags.count == 0
        viewSuggestion.hidden = !show
        tableView.hidden = show
    }

}

extension TagsViewController: ZFTokenFieldDelegate, ZFTokenFieldDataSource {
    func lineHeightForTokenInField(tokenField: ZFTokenField) -> CGFloat {
        return 26
    }
    
    func numberOfTokenInField(tokenField: ZFTokenField!) -> UInt {
        return UInt(tags.count)
    }
    
    func tokenField(tokenField: ZFTokenField!, viewForTokenAtIndex index: UInt) -> UIView! {
        let lbl = UILabel()
        lbl.textAlignment = .Center
        lbl.text = tags[Int(index)]
        var size = lbl.sizeThatFits(CGSize(width: tokenField.width, height: 33))
        size.width += 10
        size.height = 26
        lbl.size = size
        lbl.backgroundColor = UIColor ( red: 0.0, green: 0.4196, blue: 0.6078, alpha: 1.0 )
        lbl.textColor = UIColor.whiteColor()
        lbl.layer.cornerRadius = 6
        lbl.clipsToBounds = true
        return lbl
    }
    
    func tokenMarginInTokenInField(tokenField: ZFTokenField) -> CGFloat {
        return 5
    }
    
    func tokenField(tokenField: ZFTokenField, didReturnWithText text: String) {
        let tag = text.trimmed()
        if !tag.isEmpty {
            tags.append(text)
            tokenField.reloadData()
            
            updateTagsHeight()
        } else {
            view.endEditing(true)
        }
        showHideSuggestions()
    }
    
    func tokenField(tokenField: ZFTokenField!, didRemoveTokenAtIndex index: UInt) {
        tags.removeAtIndex(Int(index))
        updateTagsHeight()
        
        showHideSuggestions()
    }
    
    func tokenFieldShouldEndEditing(textField: ZFTokenField) -> Bool {
        return true
    }
    
}
