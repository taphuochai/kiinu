//
//  PaymentRequestViewController.swift
//  kiinu
//
//  Created by Duc iOS on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit
import SDWebImage

class PaymentRequestViewController: BaseVC {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnRequest: UIButton!
    
    @IBAction func closeButtonTouch(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Request payment"

        btnRequest.layer.cornerRadius = 4
        btnRequest.clipsToBounds = true
        
        self.imgView.layer.cornerRadius = self.imgView.width/2
        self.imgView.clipsToBounds = true
        self.imgView.sd_setImageWithURL(NSURL(string: "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/11150969_1111099512238595_195493537841130071_n.jpg?oh=b32a0d4568e8774b02bccead8010e863&oe=5813A31E"))
    }

    @IBAction func requestButtonTouch(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        let user = User(id: ShopID, image: "", name: "")
        
        let message = ButtonsMessage(text: "You have been requested to pay $20 to kitfe ?", buttons: ["YES", "NO"], user: user)
        FBHelper.sendPayment()
        
        NSNotificationCenter.defaultCenter().postNotificationName("SEND_MESSAGE", object: message)        
    }
}
