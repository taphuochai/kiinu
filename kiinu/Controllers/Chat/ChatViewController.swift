//
//  ChatViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import UIKit

class ChatViewController: BaseVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomCollectionViewCosntraint: NSLayoutConstraint!
    
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var bottomReplyViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    
    var messages: Array<Message> = []
    
    override func viewDidLoad() {
        self.title = "Nguyễn Bảo Khang"
        
        self.sendButton.layer.cornerRadius = 4
        self.sendButton.clipsToBounds = true
        
        // Dữ liệu fake
        let image1 = "https://scontent.xx.fbcdn.net/v/t1.0-1/c44.44.544.544/s50x50/316295_10151906553973056_2129080216_n.jpg?oh=e3ddd13e52080ecd5154acf6c7a0db39&oe=58125CA4"
        
        // Setup data
        let user = User(id: MessengerID, image: image1, name: "Tạ Phước Hải")
        let shopUser = User(id: ShopID, image: image1, name: "Tạ Phước Hải")
        
        let ms1 = Message (text: "Hi, today is my wife’s birthday. I would like to buy a dress for her. Do you have any recommendation?", user: user)
        self.messages.append(ms1)
//        let ms2 = Message (text: "Can I  help you ?", user: shopUser)
//        self.messages.append(ms2)
        
        // Product data
//        let pr1 = Product(name: "Sản phẩm 1", imageUrl: "http://images.apple.com/support/products/images/iphone-hero.jpg", price: 10000, desc: "")
//        let pr2 = Product(name: "Sản phẩm 2", imageUrl: "http://images.apple.com/support/products/images/iphone-hero.jpg", price: 10000, desc: "")
//        let pr3 = Product(name: "Sản phẩm 3", imageUrl: "http://images.apple.com/support/products/images/iphone-hero.jpg", price: 10000, desc: "")
//        let pr4 = Product(name: "Sản phẩm 4", imageUrl: "http://images.apple.com/support/products/images/iphone-hero.jpg", price: 10000, desc: "")
//        let ms3 = Products(text: "", user: user)
//        ms3.products = [pr1, pr2, pr3, pr4]
//        self.messages.append(ms3)
        
        self.setupNotification()
    }
    
    @IBAction func sendButtonTouch(sender: AnyObject) {
        let user = User(id: ShopID, image: "", name: "")
        let message = Message(text: self.textField.text!, user: user)
        self.textField.text = ""
        
        FBHelper.sendTextMessage(message)
        
        self.addMessage(message)
    }
    
    func addMessage(message: Message) {
        if message is ButtonsMessage {
            //xFBHelper.sendTextMessageAndButton(message as! ButtonsMessage)
        } else {
            //FBHelper.sendTextMessage(message)
        }
        
        self.messages.append(message)
        self.collectionView.insertItemsAtIndexPaths([NSIndexPath(forRow: self.messages.count - 1, inSection: 0)])
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.collectionView.scrollToItemAtIndexPath(NSIndexPath(forRow: self.messages.count - 1, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Bottom, animated: true)
        }
    }
}

extension ChatViewController
{
    func setupNotification() {
        
        // Add a message
        NSNotificationCenter.defaultCenter().addObserverForName("NEW_MESSAGE", object: nil, queue: nil) {
            (notification) in
            let message = notification.object as? Message
            
            self.messages.append(message!)
            
            self.collectionView.insertItemsAtIndexPaths([NSIndexPath(forRow: self.messages.count - 1, inSection: 0)])
        }
        
        // send a message
        NSNotificationCenter.defaultCenter().addObserverForName("SEND_MESSAGE", object: nil, queue: nil) {
            (notification) in
            let message = notification.object
            
            self.addMessage(message as! Message)
        }
        
        // Keyboard
        NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillShowNotification, object: nil, queue: nil) {
            (notification) in
            
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                self.bottomCollectionViewCosntraint.constant = -keyboardSize.height - 49 - self.replyView.height
                self.bottomReplyViewConstraint.constant = -keyboardSize.height
                
                UIView.animateWithDuration(0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillHideNotification, object: nil, queue: nil) {
            (notification) in
            self.bottomCollectionViewCosntraint.constant = -49 - self.replyView.height
            self.bottomReplyViewConstraint.constant = 0
            
            UIView.animateWithDuration(0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
        self.view.endEditing(true)
    }
}

extension ChatViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if (velocity.y <= -1.0) {
            self.view.endEditing(true)
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let message = self.messages[indexPath.row]
        
        var cell: ChatCollectionViewCell?
        
        if message.type == MessageType.Text {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("TextChatCollectionViewCell", forIndexPath: indexPath) as! TextChatCollectionViewCell
        } else if message.type == MessageType.Products {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductsChatCollectionViewCell", forIndexPath: indexPath) as! ProductsChatCollectionViewCell
        } else if message.type == MessageType.Image {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageChatCollectionViewCell", forIndexPath: indexPath) as! ImageChatCollectionViewCell
        } else if message.type == MessageType.Buttons {
            
            let bt = message as! ButtonsMessage
            if bt.buttons?.count == 3 {
                cell = collectionView.dequeueReusableCellWithReuseIdentifier("ButtonsCollectionViewCell", forIndexPath: indexPath) as! ButtonsCollectionViewCell
            } else {
                cell = collectionView.dequeueReusableCellWithReuseIdentifier("Buttons2CollectionViewCell", forIndexPath: indexPath) as! Buttons2CollectionViewCell
            }
        }
        
        cell?.message = message
        cell?.layoutMessage()
        
        return cell!
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let message = self.messages[indexPath.row]
        
        if message.type == MessageType.Text {
            if message.text?.characters.count > 20 {
                return CGSizeMake(ScreenSize.Width, 90)
            }
            if message.text?.characters.count > 100 {
                return CGSizeMake(ScreenSize.Width, 120)
            }
            return CGSizeMake(ScreenSize.Width, 50)
        } else if message.type == MessageType.Products {
            return CGSizeMake(ScreenSize.Width, 320)
        } else if message.type == MessageType.Image {
            return CGSizeMake(ScreenSize.Width, 150)
        }
        
        // Button
        let bt = message as! ButtonsMessage
        if bt.buttons?.count == 3 {
            return CGSizeMake(ScreenSize.Width, 210)
        }
        return CGSizeMake(ScreenSize.Width, 190)
    }
}

extension ChatViewController: UITextFieldDelegate
{
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true
    }
}