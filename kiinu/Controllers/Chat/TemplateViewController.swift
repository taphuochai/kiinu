//
//  TemplateViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class TemplateViewController: BaseVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var textView: UITextView!    
    @IBOutlet weak var addQuickChatView: UIView!
    
    
    override func viewDidLoad() {
        self.title = "QUICK SUPPORT CHAT"
        
        self.addQuickChatView.layer.cornerRadius = 4
        self.addQuickChatView.clipsToBounds = true
        
        self.textView.layer.borderColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.00).CGColor
        self.textView.layer.borderWidth = 1.0
        self.textView.layer.cornerRadius = 4
    }
    
    @IBAction func closeQuichatViewTouch(sender: AnyObject) {
        UIView.animateWithDuration(0.2) { 
            self.addQuickChatView.alpha = 0
        }
    }
    
    @IBAction func addQuickChatButtonTouch(sender: AnyObject) {
        UIView.animateWithDuration(0.2) {
            self.addQuickChatView.alpha = 1
        }
    }
    
    @IBAction func doneAddQuickChatButtonTouch(sender: AnyObject) {
        UIView.animateWithDuration(0.2) {
            self.addQuickChatView.alpha = 0
        }
    }
    
    @IBAction func closeButtonTouch(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension TemplateViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (section == 0) {
            return 2
        }
        return 4
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return collectionView.dequeueReusableCellWithReuseIdentifier("Cell1", forIndexPath: indexPath)
            }
            return collectionView.dequeueReusableCellWithReuseIdentifier("Cell2", forIndexPath: indexPath)
        }
        
        if indexPath.row == 0 {
            return collectionView.dequeueReusableCellWithReuseIdentifier("Cell3", forIndexPath: indexPath)
        }
        if indexPath.row == 1 {
            return collectionView.dequeueReusableCellWithReuseIdentifier("Cell4", forIndexPath: indexPath)
        }
        if indexPath.row == 2 {
            return collectionView.dequeueReusableCellWithReuseIdentifier("Cell5", forIndexPath: indexPath)
        }
        
        return collectionView.dequeueReusableCellWithReuseIdentifier("Cell6", forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "TemplateHeaderResuableView", forIndexPath: indexPath) as! TemplateHeaderResuableView
        if indexPath.section == 0 {
            headerView.label.text = "POPULAR MESSENGER"
        } else {
            headerView.label.text = "IMAGE / TEXT AND BUTTON"
        }
        return headerView
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(150, 150)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let user = User(id: ShopID, image: "", name: "")
        
        if indexPath.section == 0 {
            let message: Message?
            if indexPath.row == 0 {
                 message = Message(text: " Sure, what kind of dress are you looking for?", user: user)
            } else {
                message = Message(text: "Hi, thank you for contacting us. How may I help you today?", user: user)
            }
            NSNotificationCenter.defaultCenter().postNotificationName("SEND_MESSAGE", object: message)
        } else {
            var message: ButtonsMessage?
            if indexPath.row == 0 {
                message = ButtonsMessage(text: "What is the price range?", buttons: ["Below 40", "40 - 70", "Above 70"], user: user)
                FBHelper.sendPrice()
            }
            if indexPath.row == 1 {
                message = ButtonsMessage(text: "What is her/his size?", buttons: ["S", "M", "L"], user: user)
                FBHelper.sendSizes()
            }
            if indexPath.row == 2 {
                message = ButtonsMessage(text: "What is the style?", buttons: ["Maxi and midi", "Shift dresses", "Denim dresses"], user: user)
                FBHelper.sendStyles()
            }
            if indexPath.row == 3 {
                message = ButtonsMessage(text: "You have been requested to pay $20 to kitfe ?", buttons: ["YES", "NO"], user: user)
                FBHelper.sendPayment()
            }
            NSNotificationCenter.defaultCenter().postNotificationName("SEND_MESSAGE", object: message!)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
