//
//  StoreViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class StoreViewController: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Extension store"
    }
    
    @IBAction func closeButtonTouch(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}