//
//  ProductViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ProductsViewController: BaseVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedIndex = Dictionary<Int, Bool> ()
    
    var products: Array<Product> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Products"
        
        // Init product
        let pr1 = Product(name: "TFNC WEDDING High Neck Maxi Dress with Frills",
                          imageUrl: "http://forwomenall.com/wp-content/uploads/parser/black-dress-for-women-1.jpg",
                          price: 55,
                          desc: "")
        let pr2 = Product(name: "TFNC WEDDING Cold Shoulder Wrap Front Maxi Dress",
                          imageUrl: "http://g01.a.alicdn.com/kf/HTB1BysnKpXXXXciXpXXq6xXFXXX8/Hollow-Out-Short-Sleeves-Black-Dress-Slim-Tight-Dress-Women-Summer-Dresses-Sexy-Party-Dresses.jpg",
                          price: 55, desc: "")
        let pr3 = Product(name: "Native Youth Button Front Shirt Dress",
                          imageUrl: "https://s-media-cache-ak0.pinimg.com/originals/ef/a7/f4/efa7f4480187cef84da8d8ae252111b9.jpg",
                          price: 44, desc: "")
        let pr4 = Product(name: "ASOS Off Shoulder Sweat Dress",
                          imageUrl: "http://g01.a.alicdn.com/kf/HTB1TvXJIXXXXXcCXFXXq6xXFXXX1/Vestido-de-festa-2015-Summer-Style-Women-font-b-Elegant-b-font-Evening-Party-Casual-Plus.jpg",
                          price: 22, desc: "")
        let pr5 = Product(name: "TIE WAIST SHIRT DRESS",
                          imageUrl: "http://fancy-day.com/wp-content/uploads/2016/04/59694f42a356e2a.jpg",
                          price: 35, desc: "")
        
        self.products.append(pr1);
        self.products.append(pr2);
        self.products.append(pr3);
        self.products.append(pr4);
        self.products.append(pr5);
    }
    
    @IBAction func closeButtonTouch(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func sendButtonTouch(sender: AnyObject) {
        FBHelper.sendProducts()
        
        // Add products
        let shopUser = User(id: ShopID, image: "", name: "Tạ Phước Hải")
        // Product data
        let ms3 = Products(text: "", user: shopUser)
        ms3.products = [self.products[0], self.products[1], self.products[2], self.products[3], self.products[4]]
        NSNotificationCenter.defaultCenter().postNotificationName("SEND_MESSAGE", object: ms3)                
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension ProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HotProductCollectionViewCell", forIndexPath: indexPath) as! HotProductCollectionViewCell
        if self.selectedIndex[indexPath.row] == true {
            cell.checkImage.alpha = 1
        } else {
            cell.checkImage.alpha = 0
        }
        cell.product = self.products[indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((ScreenSize.Width - 30)/2, 290)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndex[indexPath.row] = self.selectedIndex[indexPath.row] == true ? false : true
        self.collectionView.reloadItemsAtIndexPaths([indexPath])
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "HeaderView", forIndexPath: indexPath) as! ProductHeaderResuableView
        return headerView
    }
    
}