//
//  BaseVC.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addAddBarButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "home_icon_bubble_quick_add"), style: .Plain, target: self, action: #selector(BaseVC.didTapAdd))
    }
    
    func addSearchBarButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "home_search_icon"), style: .Plain, target: self, action: #selector(BaseVC.didTapSearch))
    }
    
    func didTapAdd(){}
    func didTapSearch(){}

}
