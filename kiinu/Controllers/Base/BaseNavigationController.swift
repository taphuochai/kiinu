//
//  BaseNavigationController.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit
import BTUtils

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.barTintColor = UIColor ( red: 0.0, green: 0.6235, blue: 0.9098, alpha: 1.0 )
        navigationBar.tintColor = UIColor.whiteColor()
        navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont.systemFontOfSize(17),
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]
        navigationBar.setBackgroundImage(BTUtils.imageWithColor(UIColor ( red: 0.0, green: 0.6235, blue: 0.9098, alpha: 1.0 ), andSize: CGSize(width: 1, height: 1)), forBarMetrics: .Default)
        navigationBar.translucent = false
        navigationBar.shadowImage = UIImage()
    }

}
