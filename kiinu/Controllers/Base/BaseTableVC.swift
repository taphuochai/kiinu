//
//  BaseTableVC.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit

class BaseTableVC: BaseVC {
    
    @IBOutlet lazy var tableView: UITableView! = {
        let tableView = UITableView()
        self.view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges()
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tableView = tableView {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorInset = UIEdgeInsetsZero
        }
    }
    
}

extension BaseTableVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return UITableViewCell(style: .Subtitle, reuseIdentifier: "Cell")
    }
    
}
