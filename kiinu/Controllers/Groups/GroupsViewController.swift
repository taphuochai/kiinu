//
//  GroupsViewController.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit
import KLCPopup

class GroupsViewController: BaseCollectionViewController, UIActionSheetDelegate {
    
    lazy var groups: [[String: String]] = {
        var groups = [[String: String]]()
        
        groups.append(["image": "group_icon_main_1", "name": "New user"])
        groups.append(["image": "group_icon_main_2", "name": "Return user"])
        groups.append(["image": "group_icon_main_3", "name": "Complaint"])
        groups.append(["image": "group_icon_main_4", "name": "Lead"])
        groups.append(["image": "group_icon_main_5", "name": "Referral"])
        return groups
    }()
    
    var popup: KLCPopup?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "GROUPS"

        addSearchBarButton()
        addAddBarButton()
        
        let width = (view.width-5)/2-10
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSizeMake(width, width*128/165)
        flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 5, right: 5)
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 10
        
        collectionView.collectionViewLayout = flowLayout
    }
    
    override func didTapAdd() {
        showAddEdit(nil)
    }
    
    func showAddEdit(group: [String: String]?) {
        let contentView = AddEditGroupView.instantiateFromNib()
        contentView.group = group
        
        popup = KLCPopup(contentView: contentView)
        popup?.shouldHandleKeyboard = true
        
        contentView.doneBlock = { [weak self] (oldGroup, newGroup) in
            self?.popup?.dismiss(true)
            
            if let oldGroup = oldGroup, let idx = self?.groups.indexOf({ $0 == oldGroup }) {
                self?.groups[idx] = newGroup
                self?.collectionView.reloadItemsAtIndexPaths([NSIndexPath(forRow: idx, inSection:  0)])
            } else {
                self?.groups.append(newGroup)
                self?.collectionView.insertItemsAtIndexPaths([NSIndexPath(forRow: self!.groups.count-1, inSection: 0)])
            }
        }
        contentView.closeBlock = { [weak self] in
            self?.popup?.dismiss(true)
        }
        popup?.show()
    }
    
    override func didTapSearch() {
        
    }

}

extension GroupsViewController { // UICollectionViewDelegate, UICollectionViewDataSource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groups.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GroupCell", forIndexPath: indexPath) as! GroupCell
        cell.group = groups[indexPath.item]
        cell.editGroupBlock = { [weak self] group in
            self?.showAddEdit(group)
        }
        cell.removeGroupBlock = { [weak self] group in
            if let idx = self?.groups.indexOf({ $0 == group }) {
                self?.groups.removeAtIndex(idx)
                
                self?.collectionView.deleteItemsAtIndexPaths([NSIndexPath(forRow: idx, inSection: 0)])
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("listSegue", sender: nil)
    }
}
