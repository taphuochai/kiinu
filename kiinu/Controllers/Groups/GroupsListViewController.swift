//
//  GroupsListViewController.swift
//  kiinu
//
//  Created by Duc iOS on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit

class GroupsListViewController: BaseTableVC {
    lazy var groups: [[String: String]] = {
        var groups = [[String: String]]()
        for i in 0..<5 {
            groups.append([
                "image": "group_icon_main_\(i+1)",
                "name": Lorem.words(Int(arc4random_uniform(3)+1))
                ])
        }
        return groups
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "GROUPS"
        
        tableView.rowHeight = 72
        
        addAddBarButton()
    }
    
    override func didTapAdd() {
        //
    }
}

extension GroupsListViewController { // TableView
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(String(GroupListCell)) as! GroupListCell
        
        let group = groups[indexPath.row]
        cell.lblName.text = group["name"]
        cell.imgView.image = UIImage(named: group["image"]!)
        return cell
    }
}
