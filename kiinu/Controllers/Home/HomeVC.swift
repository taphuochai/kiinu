//
//  HomeVC.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import UIKit
import KLCPopup

class HomeVC: BaseTableVC {
    var conversations = [[String: AnyObject]]()
    var popup: KLCPopup?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "HOME"
        
        tableView.height = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 102
        
        var conversation = [String: AnyObject]()
        conversation["name"] = "Nguyen Bao Khang"
        conversation["message"] = "Hi, today is my wife’s birthday. I would like to buy a dress for her. Do you have any recommendation?"
        conversation["time"] = "09:41"
        conversation["avatar"] = "https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/10177923_10204308913616297_6340095400753142796_n.jpg?oh=c0875f2e3dac3c3592427880010caf88&oe=581EB05F"
        conversations.append(conversation)
        
        let avatars = ["zeldman", "marcogomes", "ripplemdk", "adellecharles", "tomaslau"]
        for avatar in avatars {
            var conversation = [String: AnyObject]()
            conversation["name"] = Lorem.name
            conversation["message"] = Lorem.sentences(5)
            conversation["time"] = "09:41"
            conversation["avatar"] = "https://s3.amazonaws.com/uifaces/faces/twitter/\(avatar)/128.jpg"
            conversations.append(conversation)
        }
        
        tableView.reloadData()
        
        if self.navigationController?.viewControllers.count > 1 {
            
        } else {
            addSearchBarButton()
        }
    }

}

extension HomeVC { // UITableViewDelegate + DataSource

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.textColor = UIColor ( red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0 )
        label.font = UIFont.systemFontOfSize(11)
        
        switch section {
        case 0: label.text = "UNREAD"
        case 1: label.text = "RECENT"
        default: label.text = "BOTS"
        }
        
        let view = UIView()
        view.addSubview(label)
        label.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsets(top: 0, left: 15, bottom: 4, right: 4), excludingEdge: .Top)
        
        view.backgroundColor = UIColor ( red: 0.949, green: 0.949, blue: 0.949, alpha: 1.0 )
        return view
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCellWithIdentifier("HomeCell") as! HomeCell
        cell.conversation = conversations[indexPath.row]
        cell.addGroupBlock = { [weak self] conversation in
            let vc = self?.storyboard?.instantiateViewControllerWithIdentifier(String(GroupsListViewController)) as! GroupsListViewController
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("chatSegue", sender: nil)
    }
}
