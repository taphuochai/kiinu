//
//  UberViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class UberViewController: UIViewController {
    
    @IBAction func closeButtonTouch(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func requestButtonTouch(sender: AnyObject) {
        
        let user = User(id: ShopID, image: "", name: "")
        let message = ButtonsMessage (text: "You order will be delivered by Uber. Sebastian (4.9 stars) from Uber will arrive in 45 minutes in a Honda Civic, license plate FA851-57", buttons: ["View Map", "Call Driver"], user: user)
        
        NSNotificationCenter.defaultCenter().postNotificationName("SEND_MESSAGE", object: message)
        
        FBHelper.sendUrber();
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}