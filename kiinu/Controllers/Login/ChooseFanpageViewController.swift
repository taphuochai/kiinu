//
//  ChooseFanpageViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class ChooseFanpageViewController: BaseVC {
    
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nextButton.layer.cornerRadius = 4
        self.nextButton.layer.borderWidth = 1
        self.nextButton.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    @IBAction func chooseFanpageButtonTouch(sender: AnyObject) {
        let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("BaseTabBarController")
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window!.rootViewController = home
    }
}

extension ChooseFanpageViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let string = Lorem.words(4)
        return NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }

    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Lorem.words(4)
    }
}