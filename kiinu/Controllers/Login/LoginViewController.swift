
//
//  LoginViewController.swift
//  kiinu
//
//  Created by taphuochai on 7/31/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class LoginViewController: BaseVC {
    
    @IBOutlet weak var loginWithFacebookButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loginWithFacebookButton.layer.cornerRadius = self.loginWithFacebookButton.height/2
        self.loginWithFacebookButton.layer.borderWidth = 1
        self.loginWithFacebookButton.layer.borderColor = UIColor.whiteColor().CGColor
    }    
}