//
//  User.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class User {
    var id: String?
    var image: String?
    var name: String?
    
    init () {
    }
    
    init (id: String, image: String, name: String) {
        self.id = id
        self.image = image
        self.name = name
    }
}