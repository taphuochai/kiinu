//
//  Message.swift
//  kiinu
//

import Foundation

enum MessageType {
    case Text
    case Products
    case Image
    case Buttons
}

class Message {
    var sender: User?
    var text: String?
    var type: MessageType = MessageType.Text
    
    init () {
    }
    
    init (text: String, user: User) {
        self.text = text
        self.sender = user
    }
}