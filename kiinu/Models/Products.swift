//
//  Products.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class Products: Message {
    var products: Array<Product> = []
    
    override init (text: String, user: User) {
        super.init(text: text, user: user)
        
        self.type = MessageType.Products
    }
}
