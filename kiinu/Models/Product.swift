//
//  Product.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class Product: Message {
    var name: String?
    var imageUrl: String?
    var price: Int?
    var desc: String?
    
    init(name: String?, imageUrl: String?, price: Int?, desc: String?) {
        super.init()
        
        self.type = MessageType.Products
        
        self.name = name
        self.imageUrl = imageUrl
        self.price = price
        self.desc = desc
    }
}