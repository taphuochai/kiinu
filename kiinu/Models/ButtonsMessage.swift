//
//  ButtonsMessage.swift
//  kiinu
//
//  Created by taphuochai on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation

class ButtonsMessage: Message {
    var buttons: [String]?
    
    init (text: String, buttons: [String], user: User) {
        super.init(text: text, user: user)
        
        self.type = MessageType.Buttons
        self.buttons = buttons
    }
}