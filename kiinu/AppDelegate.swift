//
//  AppDelegate.swift
//  kiinu
//
//  Created by Duc iOS on 7/30/16.
//  Copyright © 2016 FbHackers. All rights reserved.
//

import Foundation
import UIKit
import PusherSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let pusher = Pusher(key: "031aef8f164448680fb7")
        
        let channel = pusher.subscribe("test_channel")
        channel.bind("my_event", callback: { (data: AnyObject?) -> Void in
            print("message received: \(data)")
            let message = Message()
            
            let json = data as! NSDictionary
            let messageText = json["message"] as? String
            message.text = messageText
            
            if messageText != nil && messageText == "Confirmed payment" {
                /*
                let user = User()
                user.name = "Ta Phuoc Hai"
                user.id = ShopID
                message.sender = user
                
                let message = ButtonsMessage(text: "Please select a payment option", buttons: ["Facebook payment", "Apple pay", "Paypal"], user: user)
                
                NSNotificationCenter.defaultCenter().postNotificationName("NEW_MESSAGE", object: message)
                 */
            } else {
                let user = User()
                user.name = "Ta Phuoc Hai"
                user.id = json["senderId"] as? String
                message.sender = user
                
                NSNotificationCenter.defaultCenter().postNotificationName("NEW_MESSAGE", object: message)
                
                FBHelper.sendTextMessage(message)
            }
        })
        
        pusher.connect()
        
        
        return true
    }
   
}

